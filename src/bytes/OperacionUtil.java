package bytes;

public class OperacionUtil {

    public static void generarNumerosPrimos(int cantidad){

        try {

            System.out.println(StringUtil.generandoNumerosPrimos(cantidad));
            System.out.println(StringUtil.getLinea());
            
            int numero = 1;
            for (int i = 1; numero <= cantidad; i++) {
                int bandera = 0;
                for (int j = 1; j <= i; j++) {
                    if (i % j == 0) {
                        bandera++;
                    }
                }
                if (bandera == 2) {
                    System.out.println(StringUtil.getNumeroPrimo(numero, i));
                    numero++;
                }
            } 

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
