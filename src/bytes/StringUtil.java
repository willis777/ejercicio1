package bytes;

public class StringUtil {
    
    public static final String GENERANDO = " Generando ";
    public static final String NUMEROS_PRIMOS = " números primos";    
    
    public static String generandoNumerosPrimos(int cantidad){
      
        String resultado = "";
        
        try{
            
            resultado = GENERANDO + cantidad + NUMEROS_PRIMOS;
            
        }catch(Exception e){
            e.printStackTrace();
        }

        return resultado;
    }
    
    
    public static String getNumeroPrimo(int correlativo, int numero){
      
        String resultado = "";
        
        try{
            
            resultado = correlativo + ") " + numero;
            
        }catch(Exception e){
            e.printStackTrace();
        }

        return resultado;
    }
    
    public static String getLinea(){
      
        String resultado = "";
        
        try{
            
            resultado = " ---------------------------------------- ";
            
        }catch(Exception e){
            e.printStackTrace();
        }

        return resultado;
    }
    
    
    
    
    
    
}
